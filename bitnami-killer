#!/bin/bash

echo ""
echo "Bitnami-killer by Toolbox."
echo "--------------------------"
echo "This script auto-stops and removes bitnami for you"
echo ""
sleep 1s;

if [[ $UID != 0 ]]; then
    echo "Please run this script with sudo:"
    echo "sudo $0 $*"
    exit 1
fi

#-----------------------------------------------------
#     Execution modules
#-----------------------------------------------------

function remove_bitnami() {
	bitpath=$1
	echo ""
	echo "--------------------------------"
	echo "Removing Bitnami Installation..."
	echo "--------------------------------"
	echo ""

	echo "Determining server status..."
	sleep 1s
	"$bitpath"/ctlscript.sh status
	echo ""

	echo "Stopping bitnami service..."
	sleep 0.5s
	"$bitpath"/ctlscript.sh stop
	echo ""

	echo "Backing up bitnami folder to bitbackup.tar.gz."
	sleep 2s;
	echo "Just in case you realize you want it."
	sleep 2s;
	echo "To unzip, run 'tar -xzf bitbackup.tar.gz' and copy the resulting folder back into its original location"

	sleep 2s;
	tar -pczf ~/bitbackup.tar.gz "$bitpath"
	echo ""

	echo "Running uninstaller..."
	sleep 1s
	"$bitpath"/uninstall --mode unattended
	sudo rm -r ~/.bash*
	sudo rm -r ~/.profile*
	echo ""
	sleep 2s;

	echo "Running apt-update..."
	sleep 2s;
	sudo apt-get update
}

function add_user() {

	echo "Enter your new username: "
	read newuser
	sudo adduser "$newuser"
	sudo adduser "$newuser" admin
	echo "Use the command 'sudo su $newuser' to switch users, or log out and back in."
	echo ""
	sleep 2s
}




#-----------------------------------------------------
#     Main Code
#-----------------------------------------------------


if [ -d "$1" ]
then
	echo "Bitnami Directory specified: $1..."
  sleep 1s;
	if [ -e "$1/uninstall" && -e "$1/ctlscript.sh" ]
	then
		echo "Found control script and Uninstaller!"
		echo ""
		bitpath="$1"
	else
		echo "Error: Could not find bitnami directory at '$1'. You may have uninstalled it already!"
		bitpath=""
	fi
else
	echo "Checking for Bitnami installation under /opt/bitnami..."
	sleep 1s
	if [ -e "/opt/bitnami/uninstall" ] && [ -e "/opt/bitnami/ctlscript.sh" ]
	then
		echo "Found control script and Uninstaller!"
		echo ""
		bitpath="/opt/bitnami"
	else
		echo "Could not find under /opt/bitnami. Checking ~/bitnami"
		if [ -e "~/bitnami/uninstall" ] && [ -e "~/bitnami/ctlscript.sh" ]
		then
			echo "Found control script and Uninstaller!"
			echo ""
			bitpath="~/bitnami"
		else
			echo "Error: Could not autodetect bitnami installation. Try specifying the path. Or maybe you uninstalled it already?"
			bitpath=""
		fi
	fi
fi

echo ""
if [[ $bitpath != "" ]]
then 
	echo "Do you want to remove the bitnami installation? [y/n]: "
	read response

	while [ $response != "y" ] && [ $response != "n" ]
	do
		echo "Just a 'y' or 'n' will do, thanks. [y/n]: "
		read response
	done

	case "$response" in 
		 y) remove_bitnami $bitpath;;
		 n) ;;
		 *) ;;
	esac
fi

echo "Do you want to set up a new user so you don't have to log in as bitnami? [y/n]: "
read response

while [ $response != "y" ] && [ $response != "n" ]
do
	echo "Just a 'y' or 'n' will do, thanks. [y/n]: "
	read response
done

case "$response" in 
     y) add_user;;
     n) exit 1;;
     *) exit -2;;
esac

